<?php
/*
 * The following variables are available in this template:
 * - $this: the CrudCode object
*/
?>
<?php
echo "<?php\n";
$label=$this->pluralize($this->class2name($this->modelClass));
echo "
\$this->pageTitle=Yii::app()->name . ' - Управление ::NAME::';
\$this->breadcrumbs=array(
	'::BNAME::'	=>	array('index'),
	'Управление',
);\n";
?>

$this->menu = array(
	array('label'=>'Список ::NAME::', 'url'=>array('index')),
	array('label'=>'Добавить ::NAME::', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('<?php echo $this->class2id($this->modelClass); ?>-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h2>Управление ::NAME::</h2>

<p>Вы можете дополнительно использовать операторы сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> or <b>=</b>) в начале каждого поля поиска.</p>

<?php echo "<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>"; ?>

<div class="search-form" style="display:none">
<?php echo "<?php \$this->renderPartial('_search',array(
	'model'=>\$model,
)); ?>\n"; ?>
</div><!-- search-form -->

<?php echo "<?php"; ?> $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'emptyText' => 'Ничего не найдено',
	'summaryText'=>Yii::t('labels', 'Результаты {start}-{end} из {count}'),
	//'template' => '{summary}, {items} and {pager}.',
	'pager' => array(
		'class'				=> 'CLinkPager',
		'header'				=> 'Страницы:',
		'prevPageLabel'	=> 'Назад',
		'nextPageLabel'	=> 'Вперед',
	),
	'columns'=>array(
<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if(++$count==7)
		echo "\t\t/*\n";
	echo "\t\t'".$column->name."',\n";
}
if($count>=7)
	echo "\t\t*/\n";
?>
		array(
			'class'=>'CButtonColumn',
			'buttons' => array (
				'update' => array(
					'label' => 'Редактировать',
				),
				'view' => array(
					'label' => 'Просмотр',
				),
				'delete' => array(
					'label' => 'Удалить',
				),
			),
		),
	),
)); ?>