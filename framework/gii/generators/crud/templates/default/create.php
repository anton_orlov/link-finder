<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php
echo "<?php\n";
$label=$this->pluralize($this->class2name($this->modelClass));
echo "
\$this->pageTitle=Yii::app()->name . ' - Добавить ::NAME::';
\$this->breadcrumbs=array(
	'::BNAME::'	=>	array('index'),
	'Добавить',
);\n";
?>

$this->menu=array(
	array('label' => 'Список ::NAME::', 'url'=>array('index')),
	array('label' => 'Управление ::NAME::', 'url'=>array('admin')),
);
?>

<h2>Добавить ::NAME::</h2>

<?php echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>
