<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
echo "
\$this->pageTitle=Yii::app()->name . ' - Редактировать ::NAME:: '.::BFIELD::;
\$this->breadcrumbs=array(
	'::BNAME::'=>array('index'),
	::BFIELD::=>array('view','id'=>\$model->{$this->tableSchema->primaryKey}),
	'Редактировать',
);\n";
?>

$this->menu=array(
	array('label'	=>	'Список ::NAME::', 'url'=>array('index')),
	array('label'	=>	'Добавить ::NAME::', 'url'=>array('create')),
	array('label'	=>	'Просмотр ::NAME::', 'url'=>array('view', 'id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
	array('label'	=>	'Управление ::NAME::', 'url'=>array('admin')),
);
?>

<h2>Редактировать '::FIELD::'</h2>

<?php echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>