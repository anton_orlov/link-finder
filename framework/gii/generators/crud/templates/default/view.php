<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->pluralize($this->class2name($this->modelClass));
echo "
\$this->pageTitle=Yii::app()->name . ' - Просмотр ::NAME:: '.::BFIELD::;
\$this->breadcrumbs=array(
	'::BNAME::' => array('index'),
	::BFIELD::,
);\n";
?>

$this->menu=array(
	array('label'=>'Список ::NAME::', 'url'=>array('index')),
	array('label'=>'Добавить ::NAME::', 'url'=>array('create')),
	array('label'=>'Редактировать ::NAME::', 'url'=>array('update', 'id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
	array('label'=>'Удалить ::NAME::', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>),'confirm'=>'Вы уверены, что хотите удалить данный элемент?')),
	array('label'=>'Управление ::NAME::', 'url'=>array('admin')),
);
?>

<h2>Просмотр '::FIELD::'</h2>

<?php echo "<?php"; ?> $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
<?php
foreach($this->tableSchema->columns as $column)
	echo "\t\t'".$column->name."',\n";
?>
	),
)); ?>
