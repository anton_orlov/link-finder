<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'	=> dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'		=> 'Link Finder',

	// preloading 'log' component
	'preload'=>array('log'),

	 'defaultController' => 'lfQueries',
	 'language'=>'ru',

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'finder',
		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('94.244.62.173','::1'),
		),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'loginUrl'=>array('login/index')
		),
		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		// uncomment the following to use a MySQL database
		*/
		'db'=> include('db.php'),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'4eladi@gmail.com',
		'langs' => array(
			'EN'	=> 'английский',
			'AR'	=> 'арабский',
			'HU'	=> 'венгерский',
			'NL'	=> 'голландский',
			'EL'	=> 'греческий',
			'DA'	=> 'датский',
			'ES'	=> 'испанский',
			'IT'	=> 'итальянский',
			'KO'	=> 'корейский',
			'DE'	=> 'немецкий',
			'NO'	=> 'норвежский',
			'PL'	=> 'польский',
			'PT'	=> 'португальский',
			'TR'	=> 'турецкий',
			'FI'	=> 'финский',
			'FR'	=> 'французский',
			'SV'	=> 'шведский',
			'JA'	=> 'японский',
		),
	),
);
