<?php

class CronCommand extends CConsoleCommand
{
	
	private $proxy_url = 'http://www.lszzfcn.com/proxy/get2.php?key=412f26b0f3eef87ef442600a75d83b7d&types=socks4,socks5';
	private $serp_url = 'http://www.google.com.ua/search?hl=en&site=webhp&source=hp&q=';
	private $check_text = '<\/div><\/div><\/div><\/body><\/html>';
	private $pr_sleep = 5;
	private $lock_file = '';
		
	public function run () {
		define('HDOM_TYPE_ELEMENT', 1);
		define('HDOM_TYPE_COMMENT', 2);
		define('HDOM_TYPE_TEXT',    3);
		define('HDOM_TYPE_ENDTAG',  4);
		define('HDOM_TYPE_ROOT',    5);
		define('HDOM_TYPE_UNKNOWN', 6);
		define('HDOM_QUOTE_DOUBLE', 0);
		define('HDOM_QUOTE_SINGLE', 1);
		define('HDOM_QUOTE_NO',     3);
		define('HDOM_INFO_BEGIN',   0);
		define('HDOM_INFO_END',     1);
		define('HDOM_INFO_QUOTE',   2);
		define('HDOM_INFO_SPACE',   3);
		define('HDOM_INFO_TEXT',    4);
		define('HDOM_INFO_INNER',   5);
		define('HDOM_INFO_OUTER',   6);
		define('HDOM_INFO_ENDSPACE',7);
		define('DEFAULT_TARGET_CHARSET', 'UTF-8');
		define('DEFAULT_BR_TEXT', "\r\n");

		$this->lock_file = YiiBase::getPathOfAlias('application').'/runtime/cron.run.lock' ;
		$check_run = $this->run_lock ($this->lock_file);
		$this->add_tasks();
		$this->do_tasks(10);
		$this->check_pr(25);
	}
	
	public function add_tasks () {
		$db = Yii::app()->db;
		
		$undone_sites = $db->createCommand()
			->select('id, lang')
			->from('{{sites}}')
			->where('done=0')
			->queryAll();
		
		if (count($undone_sites) > 0) {
			$undone_langs = array();
			foreach ($undone_sites as $undone_site) {
				$lang = $undone_site['lang'];
				$undone_langs[] = $lang;
			}
			$undone_langs = array_unique($undone_langs);
			
			$queries = $db->createCommand()
				->select('id, lang')
				->from("{{queries}}")
				->where(array('in', 'lang', $undone_langs))
				->queryAll();
			
			$q_data = array();
			if (count($queries) > 0) {
				foreach ($queries as $query) {
					$q_data[$query['lang']][] = $query['id'];
				}
				
				foreach ($undone_sites as $undone_site) {
					$site_id = $undone_site['id'];
					$lang = $undone_site['lang'];
					if (isset($q_data[$lang])) {
						foreach ($q_data[$lang] as $q_id) {
							$cmd = $db->createCommand('INSERT IGNORE INTO {{tasks}} (`site_id`, `query_id`) VALUES (:site_id, :query_id)')
							->bindParam(":site_id", $site_id)
							->bindParam(":query_id", $q_id)
							->execute();
						}
						$db->createCommand()
							->update('{{sites}}', array(
									'done' => 1
								),
								'id='.$site_id
							);
					}		
				}	
			}		
		}		
	}
	
	private function do_tasks ($limit) {
		$db = Yii::app()->db;
		
		$undone_tasks = $db->createCommand()
			->select('id, site_id, query_id')
			->from('{{tasks}}')
			->where('done=0')
			->limit($limit)
			->queryAll();
	
		if (count($undone_tasks) > 0) {
			$site_ids = array();
			$query_ids = array();
			foreach ($undone_tasks as $undone_task) {
				$site_ids[] = $undone_task['site_id'];
				$query_ids[] = $undone_task['query_id'];
			}
			$site_ids = array_unique($site_ids);
			$query_ids = array_unique($query_ids);
			
			$queries = $db->createCommand()
				->select('id, query')
				->from("{{queries}}")
				->where(array('in', 'id', $query_ids))
				->queryAll();
				
			$q_rel = array();
			foreach ($queries as $query) {
				$q_rel[$query['id']] = $query['query']; 			
			}
			
			$sites = $db->createCommand()
				->select('id, url')
				->from("{{sites}}")
				->where(array('in', 'id', $site_ids))
				->queryAll();
				
			$s_rel = array();
			foreach ($sites as $site) {
				$s_rel[$site['id']] = $site['url']; 			
			}
			
			foreach ($undone_tasks as $undone_task) {
				$task_id = $undone_task['id'];;
				$site_id = $undone_task['site_id'];
				$query_id = $undone_task['query_id'];
				$site_url = $s_rel[$site_id];
				$query = $q_rel[$query_id];
				
				echo "Making task $task_id\n";
				
				$url = $this->serp_url . urlencode("site:$site_url $query");
				
				$text = $this->fetch_url ($url, $this->check_text);
				if ($text === false)
					continue;
				$result_urls = $this->get_serp_urls ($text);
				
				$url2 = $url.'&start=10&sa=N';
				
				$text2 = $this->fetch_url ($url2, $this->check_text);
				if ($text2 === false)
					continue;
				$result_urls2 = $this->get_serp_urls ($text2);
				
				$result_urls = array_merge($result_urls, $result_urls2);
				
				$result_urls = array_unique($result_urls);

				if (count($result_urls) > 0) {
					$serp_pos = 1;
					foreach ($result_urls as $result_url) {
						echo "Found url: {$result_url}, position: {$serp_pos}\n";
						$db->createCommand()
								->insert('{{pages}}', array(
									'url'					=> $result_url, 
									'query_id'			=> $query_id,
									'site_id'			=> $site_id,
									'serp_position'	=> $serp_pos
						));
						$serp_pos++;											
					}
				}
				$db->createCommand()
					->update('{{tasks}}', array(
							'done' => 1
							),
							'id='.$task_id
						);		
			}
		}
	}
	
	private function check_pr ($limit) {
		$db = Yii::app()->db;
		$unchecked_pages = $db->createCommand()
			->select('id, url')
			->from('{{pages}}')
			->where(array('or', 'pr=-1', 'pr_recheck=1'))
			->limit($limit)
			->queryAll();
		if (count($unchecked_pages) > 0) {
			$checker = new pr_checker;
			foreach ($unchecked_pages as $unchecked_page) {
				$id = $unchecked_page['id'];
				$url = $unchecked_page['url'];
				echo "Checking pr for: $url ... ";
				$pr = $checker->getRank($url, $this->get_proxies());
				echo "Result $pr\n";
				$db->createCommand()
						->update('{{pages}}', array(
							'pr' => $pr,
							'pr_recheck' => 0,
						),
						'id='.$id
				);
				
				sleep($this->pr_sleep);
			}
		}				
	}
	
	private function get_serp_urls ($text) {
		$result = array();
		$h3s = $this->dom_get($text, 'h3.r', 1);
		foreach ($h3s as $h3) {
			if (preg_match('/<a href="([^"]+)"/', $h3, $_mtch)) {
				$result[] = $_mtch[1];
			}			
		}
		return $result;
	} 
	
	private function set_tasks () {
		;
	}
	
	private function get_tasks () {
		;
	}
	
	private function fetch_url ($url, $check_text = '', $tries = 5) {
		$r = false;
		$cnt = 0;
		while ($tries > $cnt) {
			$cnt++;
			$parser = new parser($this->proxy_url);
			$parser->set_proxy();
			//$parser->cookies = 'PREF=ID=229b5d72d42e7331:U=3f32321de90407a0:FF=0:LD=uk:NR=20:TM=1337774610:LM=1337788295:GBV=1:SG=2:S=yFgmCGVbwCTQZYn_';
			$result = $parser->get_content($url);	
			if ($result['code'] == 200 && preg_match('/'.$check_text.'/i', $result['text'])) {
				$r = $result['text'];
				break;
			} else {
				$parser->set_proxy();	
			}			
		}
		return $r;
	}
	
	private function dom_get ($html, $path, $all = false, $outer = false) {
		if ($all)
			$r = array();
		else 
			$r = false;
			
		if ($outer)
			$method = 'outertext';
		else 
			$method = 'innertext';
	
		$html = preg_replace('/\s+/', ' ', $html);
		$obj = $this->str_get_html($html);
		if (!is_object($obj)) {
			echo "Simple DOM error:\n$path\n$html\n";
			return '';
		}
			
		foreach ($obj->find($path, null, true) as $e) {
			if ($all) {
				$r[] = $e->$method;	
			} else {
				$r = $e->$method;
				break;
			}
		}
			
		$obj->clear();
		unset($obj);	
		return $r;
	}
	
	private function str_get_html($str, $lowercase=true, $forceTagsClosed=true, $target_charset = DEFAULT_TARGET_CHARSET, $stripRN=true, $defaultBRText=DEFAULT_BR_TEXT) 
	{
    $dom = new simple_html_dom(null, $lowercase, $forceTagsClosed, $target_charset, $defaultBRText);
    if (empty($str))
    {
        $dom->clear();
        return false;
    }
    $dom->load($str, $lowercase, $stripRN);
    return $dom;
	}
	
	private function get_proxies ($types = array("http", "socks4", "socks5")) {
		$result = array();
		$proxies = explode("\n", file_get_contents($this->proxy_url));
		foreach ($proxies as $proxy) {
			if (preg_match('/^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+:[0-9]+::(' . implode('|', $types) . ')$/', $proxy)) {
				$result[] = $proxy;					
			}
		}
		return $result;	
	}
	
	function run_lock ($filename = null, $exit_text = null) {
		if (!$filename) $filename = 'lock.run';
		if (!$exit_text) $exit_text = "Script is already running. Exiting...\n";
		$file = fopen($filename, 'w');
		if (flock($file, LOCK_EX | LOCK_NB))
			return $file;
		else 
			exit($exit_text);
	}		
}

?>
