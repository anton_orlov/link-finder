<?php

class parser {
	
	public $encoding = 'UTF-8';
	public $check_text = null;
	public $curl_timeout = 10;
	public $curl_proxy_timeout = 20;
	
	public $use_proxy = 0;
	private $proxies = array();
	private $proxy = null;
	
	public $use_cache = 0;
	public $cache_dir = null;
	
	public $cookies_file = null;
	public $cookies = null;
	
	
	public function __construct($proxy_path = false) {
		if ($proxy_path) {
			$proxies = explode("\n", file_get_contents($proxy_path));
			foreach ($proxies as $proxy) {
				if (preg_match('/^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+:[0-9]+::(http|socks4|socks5)$/', $proxy))
					$this->proxies[] = $proxy;					
			}
			if (count($this->proxies) == 0) {
				exit("No valid proxies found\n");				
			} else {
				$this->use_proxy = 1;			
			}		
		}
		if (isset($cookies_file))
			file_put_contents($cookies_file, '\n');
	}
	
	public function set_proxy () {
		if ($this->use_proxy) {
			shuffle($this->proxies);
			$this->proxy = explode("::", $this->proxies[0]);
			echo "Current proxy - ".$this->proxy[0].", type - ".$this->proxy[1]."\n";
		} else 
			$this->unset_proxy();	
	}
	
	public function unset_proxy () {
		$this->proxy = null;	
	}
	
	public function empty_cached ($url) {
		$url = str_replace(' ', '%20', $url);
		if ($this->use_cache && $this->cache_dir) {
			$dir = $this->cache_dir. '/'. hexdec(substr(md5($url), 0, 2));
			$filename = $dir . '/' . md5($url) . '.txt';
			if (file_exists($filename)) {
				if (unlink($filename))
					echo "{$filename} deleted from cache...\n";
				else 
					echo "Error occurred deleting {$filename}...\n";
			} else {
				echo "No cache {$filename} found...\n";	
			}
		}	
	}
			
	public function get_content ($url) {
		$url = str_replace(' ', '%20', $url);
		if ($this->use_cache && $this->cache_dir) {
			$dir = $this->cache_dir. '/'. hexdec(substr(md5($url), 0, 2));
			if (!file_exists($dir))
				mkdir($dir);
			$filename = $dir . '/' . md5($url) . '.txt';
			if (file_exists($filename)) {
				echo "Taking from disk ...\n";
				$result = file_get_contents($filename);
				return array('code' => 'cache', 'text' => $result);	
			}
		}
		$curl = new curl_http_client();
		$curl->set_user_agent('Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:11.0) Gecko/20100101 Firefox/11.0');
		//$curl->include_response_headers(TRUE);		
		if ($this->proxy) {
			$curl->set_client_ip(array($this->proxy[0], 'proxy', $this->proxy[1]));
			$timeout = $this->curl_proxy_timeout;
		} else 
			$timeout = $this->curl_timeout;
		if (isset($this->cookies)) {
			$curl->set_cookie($this->cookies);	
		}
		if (isset($cookies_file))
			$curl->store_cookies($cookies_file);
		//$curl->set_referrer();
		$curl->set_headers(array(
			'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
			'Accept-Language: en-us,en;q=0.5',
			)
		);		
		$result = $curl->fetch_url($url, $timeout);
		$code = $curl->get_http_response_code();
		$eff_url = $curl->get_effective_url();
		$eff_url_error = false;
		if ($eff_url != $url) {
			$eff_url_error = "Different destination url: $url ({$eff_url})\n";
			echo $eff_url_error;
			//file_put_contents(PS_PATH . '/eff_url_error.txt', $eff_url_error, FILE_APPEND);	
		}
		if ($code != 200)
			return array('code' => $code, 'text' => $result);
		if ($this->encoding != 'UTF-8')
			$result = iconv($this->encoding, "UTF-8", $result);
		$result = str_replace('&amp;', '&', $result);
		if ($this->use_cache && $this->cache_dir && !empty($this->check_text)) {
			if (strpos($result, $this->check_text) !== false) {
				echo "Saving on disk...\n";
				file_put_contents($filename, $result);
			}
		}
		//file_put_contents(PS_PATH. '/tmp/'. urlencode($url) . '.txt', $result);
		return array('code' => $code, 'text' => $result);				  	
	}
}

?>
