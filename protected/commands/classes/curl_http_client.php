<?php

	class curl_http_client {
		private $ch = null;
		private $debug = false;
		private $error_msg = null;
		private $use_gzip = false;

		public function __construct ($debug = false) {
			$this->debug = $debug;
			$this->init();
		}

		public function init()	{
			// initialize curl handle
			$this->ch = curl_init();

			//set various options

			//set error in case http return code bigger than 300
			curl_setopt($this->ch, CURLOPT_FAILONERROR, true);

			// allow redirects
			curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);

			// use gzip if possible
			curl_setopt($this->ch, CURLOPT_ENCODING , 'gzip, deflate');

			// do not veryfy ssl
			// this is important for windows
			// as well for being able to access pages with non valid cert
			curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
		}

		public function set_credentials($username, $password) {
			curl_setopt($this->ch, CURLOPT_USERPWD, "$username:$password");
		}
		
		public function set_headers ($array) {
			curl_setopt($this->ch, CURLOPT_HTTPHEADER, $array);
		}

		public function set_referrer($referrer_url) {
			curl_setopt($this->ch, CURLOPT_REFERER, $referrer_url);
		}

		public function set_user_agent($useragent)	{
			curl_setopt($this->ch, CURLOPT_USERAGENT, $useragent);
		}

		public function include_response_headers($value) {
			curl_setopt($this->ch, CURLOPT_HEADER, $value);
		}

		public function store_cookies($cookie_file) {
			// use cookies on each request (cookies stored in $cookie_file)
			curl_setopt ($this->ch, CURLOPT_COOKIEJAR, $cookie_file);
			curl_setopt ($this->ch, CURLOPT_COOKIEFILE, $cookie_file);
		}

		public function set_cookie($cookie) {
			curl_setopt ($this->ch, CURLOPT_COOKIE, $cookie);
		}

		public function set_client_ip ($data)	{
			$this->current_ip = $data[0];
			if ($data[1] == "local") {
				curl_setopt($this->ch, CURLOPT_INTERFACE, $this->current_ip);
			} elseif ($data[1] == "proxy") {
				curl_setopt($this->ch, CURLOPT_PROXY, $this->current_ip);
				if ($data[2] == "http")
					curl_setopt($this->ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
				elseif ($data[2]  == "socks4")
					curl_setopt($this->ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS4);
				elseif ($data[2] == "socks5")
					curl_setopt($this->ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
			}
		}
		
		public function get_headers ($url, $redirects = true, $timeout = 10) {
			curl_setopt($this->ch, CURLOPT_URL, $url);
			curl_setopt($this->ch, CURLOPT_HEADER, true);
			curl_setopt($this->ch, CURLOPT_NOBODY, true);
			curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
			//allow 404 or other error answers
			curl_setopt($this->ch, CURLOPT_FAILONERROR, false);
			curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, $redirects);
			curl_setopt($this->ch, CURLOPT_TIMEOUT, $timeout);
			$result = curl_exec($this->ch);
			$headers = explode("\r\n", $result);
			$c = -1;
			foreach ($headers as $header) {
				$header = trim($header);
				if ($header != '') {
					if (substr($header, 0, 7) == 'HTTP/1.') {
						$c++;
						$out[$c][] = $header;					
					} else 
						$out[$c][] = $header;					
				}				
			}
			return $out;
		}
		
		/**
		 * Send post data to target URL
		 * return data returned from url or false if error occured
		 * @param string url
		 * @param mixed post data (assoc array ie. $foo['post_var_name'] = $value or as string like var=val1&var2=val2)
		 * @param int timeout in sec for complete curl operation (default 10)
		 * @return string data
		 * @access public
		 */
		public function send_post_data($url, $postdata, $timeout = 10) {
			//set various curl options first

			// set url to post to
			curl_setopt($this->ch, CURLOPT_URL,$url);

			// return into a variable rather than displaying it
			curl_setopt($this->ch, CURLOPT_RETURNTRANSFER,true);

			//set curl function timeout to $timeout
			curl_setopt($this->ch, CURLOPT_TIMEOUT, $timeout);

			//set method to post
			curl_setopt($this->ch, CURLOPT_POST, true);


			//generate post string
			$post_array = array();
			if (is_array($postdata)) {
				foreach($postdata as $key=>$value) {
					$post_array[] = urlencode($key) . "=" . urlencode($value);
				}
				$post_string = implode("&",$post_array);
				if($this->debug) {
					echo "Url: $url\nPost String: $post_string\n";
				}
			}
			else {
				$post_string = $postdata;
			}

			// set post string
			curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_string);


			//and finally send curl request
			$result = curl_exec($this->ch);

			if(curl_errno($this->ch))
			{
				if($this->debug)
				{
					echo "Error Occured in Curl\n";
					echo "Error number: " .curl_errno($this->ch) ."\n";
					echo "Error message: " .curl_error($this->ch)."\n";
				}

				return false;
			}
			else
			{
				return $result;
			}
		}
		
		public function raw_request ($url, $timeout) {
			$f = fopen('request.txt', 'w+');			
			curl_setopt($this->ch, CURLOPT_URL, $url);
			curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($this->ch, CURLOPT_TIMEOUT, $timeout);
			curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Accept:', 'Connection: close'));
			curl_setopt($this->ch, CURLOPT_VERBOSE, 1);
			curl_setopt($this->ch, CURLOPT_STDERR, $f);  
    		$result = curl_exec($this->ch);
			fclose($f);
			if(curl_errno($this->ch))
			{
				if($this->debug)
				{
					echo "Error Occured in Curl\n";
					echo "Error number: " .curl_errno($this->ch) ."\n";
					echo "Error message: " .curl_error($this->ch)."\n";
				}

				return false;
			}
			else
			{
				return $result;
			}		
		}	
		

		public function fetch_url($url, $timeout = 10) {
			// set url to post to
			curl_setopt($this->ch, CURLOPT_URL, $url);

			//set method to get
			curl_setopt($this->ch, CURLOPT_HTTPGET, true);

			// return into a variable rather than displaying it
			curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

			//set curl function timeout to $timeout
			curl_setopt($this->ch, CURLOPT_TIMEOUT, $timeout);

			//and finally send curl request
			$result = curl_exec($this->ch);

			if(curl_errno($this->ch))
			{
				if($this->debug)
				{
					echo "Error Occured in Curl\n";
					echo "Error number: " .curl_errno($this->ch) ."\n";
					echo "Error message: " .curl_error($this->ch)."\n";
				}

				return false;
			}
			else
			{
				return $result;
			}
		}

		/**
		 * Fetch data from target URL
		 * and store it directly to file
		 * @param string url
		 * @param resource value stream resource(ie. fopen)
		 * @param string ip address to bind (default null)
		 * @param int timeout in sec for complete curl operation (default 5)
		 * @return boolean true on success false othervise
		 * @access public
		 */
		public function fetch_into_file($url, $filename, $timeout = 10) {
			
			// set url to post to
			curl_setopt($this->ch, CURLOPT_URL, $url);

			//set method to get
			curl_setopt($this->ch, CURLOPT_HTTPGET, true);
			
			curl_setopt($this->ch, CURLOPT_HEADER, 0);

			curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
			
			curl_setopt($this->ch, CURLOPT_BINARYTRANSFER, true);
			
			//set curl function timeout to $timeout
			curl_setopt($this->ch, CURLOPT_TIMEOUT, $timeout);

			//and finally send curl request
			$result = curl_exec($this->ch);
			
			file_put_contents($filename, $result);
			
			//fclose($fp);

			if(curl_errno($this->ch))
			{
				if($this->debug)
				{
					echo "Error Occured in Curl\n";
					echo "Error number: " .curl_errno($this->ch) ."\n";
					echo "Error message: " .curl_error($this->ch)."\n";
				}

				return false;
			}
			else
			{
				return true;
			}
		}

		/**
		 * Send multipart post data to the target URL
		 * return data returned from url or false if error occured
		 * (contribution by vule nikolic, vule@dinke.net)
		 * @param string url
		 * @param array assoc post data array ie. $foo['post_var_name'] = $value
		 * @param array assoc $file_field_array, contains file_field name = value - path pairs
		 * @param string ip address to bind (default null)
		 * @param int timeout in sec for complete curl operation (default 30 sec)
		 * @return string data
		 * @access public
		 */
		public function send_multipart_post_data($url, $postdata, $file_field_array=array(), $timeout = 30) {
			//set various curl options first

			// set url to post to
			curl_setopt($this->ch, CURLOPT_URL, $url);

			// return into a variable rather than displaying it
			curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

			//set curl function timeout to $timeout
			curl_setopt($this->ch, CURLOPT_TIMEOUT, $timeout);

			//set method to post
			curl_setopt($this->ch, CURLOPT_POST, true);

			// disable Expect header
			// hack to make it working
			$headers = array("Expect: ");
			curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

			// initialize result post array
			$result_post = array();

			//generate post string
			$post_array = array();
			$post_string_array = array();
			if(!is_array($postdata))
			{
				return false;
			}

			foreach($postdata as $key=>$value)
			{
				$post_array[$key] = $value;
				$post_string_array[] = urlencode($key)."=".urlencode($value);
			}

			$post_string = implode("&",$post_string_array);


			if($this->debug)
			{
				echo "Post String: $post_string\n";
			}

			// set post string
			//curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_string);


			// set multipart form data - file array field-value pairs
			if(!empty($file_field_array))
			{
				foreach($file_field_array as $var_name => $var_value)
				{
					if(strpos(PHP_OS, "WIN") !== false) $var_value = str_replace("/", "\\", $var_value); // win hack
					$file_field_array[$var_name] = "@".$var_value;
				}
			}

			// set post data
			$result_post = array_merge($post_array, $file_field_array);
			curl_setopt($this->ch, CURLOPT_POSTFIELDS, $result_post);


			//and finally send curl request
			$result = curl_exec($this->ch);

			if(curl_errno($this->ch))
			{
				if($this->debug)
				{
					echo "Error Occured in Curl\n";
					echo "Error number: " .curl_errno($this->ch) ."\n";
					echo "Error message: " .curl_error($this->ch)."\n";
				}

				return false;
			}
			else
			{
				return $result;
			}
		}

		/**
		 * Get last URL info
		 * usefull when original url was redirected to other location
		 * @access public
		 * @return string url
		 */
		public function get_effective_url() {
			return curl_getinfo($this->ch, CURLINFO_EFFECTIVE_URL);
		}

		/**
		 * Get http response code
		 * @access public
		 * @return int
		 */
		public function get_http_response_code() {
			return curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
		}

		/**
		 * Return last error message and error number
		 * @return string error msg
		 * @access public
		 */
		public function get_error_msg() {
			$err = "Error number: " .curl_errno($this->ch) ."\n";
			$err .="Error message: " .curl_error($this->ch)."\n";

			return $err;
		}
		
		public function reset_options () {
			curl_close($this->ch);
			$this->init();	
		}

		function __destruct () {
			//close curl session and free up resources
			curl_close($this->ch);
		}
	}
?>
