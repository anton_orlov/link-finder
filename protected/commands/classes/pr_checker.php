<?php

class pr_checker {

  // Track the instance
  private static $instance;

  // Constructor
  function getRank($page, $proxies = array()) {
    // Create the instance, if one isn't created yet
    if(!isset(self::$instance)) {
      self::$instance = new self();
    }
    // Return the result
    return self::$instance->check($page);
  }

  // Convert string to a number
  function stringToNumber($string,$check,$magic) {
    $int32 = 4294967296;  // 2^32
      $length = strlen($string);
      for ($i = 0; $i < $length; $i++) {
          $check *= $magic;
          //If the float is beyond the boundaries of integer (usually +/- 2.15e+9 = 2^31),
          //  the result of converting to integer is undefined
          //  refer to http://www.php.net/manual/en/language.types.integer.php
          if($check >= $int32) {
              $check = ($check - $int32 * (int) ($check / $int32));
              //if the check less than -2^31
              $check = ($check < -($int32 / 2)) ? ($check + $int32) : $check;
          }
          $check += ord($string{$i});
      }
      return $check;
  }

  // Create a url hash
  function createHash($string) {
    $check1 = $this->stringToNumber($string, 0x1505, 0x21);
      $check2 = $this->stringToNumber($string, 0, 0x1003F);

    $factor = 4;
    $halfFactor = $factor/2;

      $check1 >>= $halfFactor;
      $check1 = (($check1 >> $factor) & 0x3FFFFC0 ) | ($check1 & 0x3F);
      $check1 = (($check1 >> $factor) & 0x3FFC00 ) | ($check1 & 0x3FF);
      $check1 = (($check1 >> $factor) & 0x3C000 ) | ($check1 & 0x3FFF);  

      $calc1 = (((($check1 & 0x3C0) << $factor) | ($check1 & 0x3C)) << $halfFactor ) | ($check2 & 0xF0F );
      $calc2 = (((($check1 & 0xFFFFC000) << $factor) | ($check1 & 0x3C00)) << 0xA) | ($check2 & 0xF0F0000 );

      return ($calc1 | $calc2);
  }

  // Create checksum for hash
  function checkHash($hashNumber)
  {
      $check = 0;
    $flag = 0;

    $hashString = sprintf('%u', $hashNumber) ;
    $length = strlen($hashString);

    for ($i = $length - 1;  $i >= 0;  $i --) {
      $r = $hashString{$i};
      if(1 === ($flag % 2)) {
        $r += $r;
        $r = (int)($r / 10) + ($r % 10);
      }
      $check += $r;
      $flag ++;
    }

    $check %= 10;
    if(0 !== $check) {
      $check = 10 - $check;
      if(1 === ($flag % 2) ) {
        if(1 === ($check % 2)) {
          $check += 9;
        }
        $check >>= 1;
      }
    }

    return '7'.$check.$hashString;
  }

  function check($page) {

    // Open a socket to the toolbarqueries address, used by Google Toolbar
    $socket = fsockopen("toolbarqueries.google.com", 80, $errno, $errstr, 30);
    //$socket = $this->fsockopen_ip("toolbarqueries.google.com", 80, $errno, $errstr, 30, '91.210.189.181');

    // If a connection can be established
    if($socket) {
      // Prep socket headers
      $out = "GET /tbr?client=navclient-auto&ch=".$this->checkHash($this->createHash($page)).
              "&features=Rank&q=info:".$page."&num=100&filter=0 HTTP/1.1\r\n";
      $out .= "Host: toolbarqueries.google.com\r\n";
      $out .= "User-Agent: Mozilla/4.0 (compatible; GoogleToolbar 2.0.114-big; Windows XP 5.1)\r\n";
      $out .= "Connection: Close\r\n\r\n";

      // Write settings to the socket
      fwrite($socket, $out);

      // When a response is received...
      $result = "";
      while(!feof($socket)) {
        $data = fgets($socket, 128);
        $pos = strpos($data, "Rank_");
        if($pos !== false){
          $pagerank = substr($data, $pos + 9);
          $result = (int)$pagerank;
        } else {
        	$result = '-';       	
        }
      }
      
      //echo $data."\n";
      // Close the connection
      fclose($socket);

      // Return the rank!
      return $result;
    }
  }
  
	private function check_proxy($page, $proxies, $tries = 5, $timeout = 30) {
		$cnt = 0; 
		$result = 0;
  		while ($cnt < $tries) {
  			$cnt++;
  			shuffle($proxies);
  			$cproxy = explode('::', $proxies[0]); 			
  			$curl = new curl_http_client(0);
  			$curl->set_user_agent('Mozilla/4.0 (compatible; GoogleToolbar 2.0.114-big; Windows XP 5.1)');
			//$curl->set_client_ip(array($cproxy[0], 'proxy', $cproxy[1]));
			echo "Current proxy: {$cproxy[1]}, {$cproxy[0]}\n";
			$url = "http://toolbarqueries.google.com/tbr?client=navclient-auto&ch=".$this->checkHash($this->createHash($page))."&features=Rank&q=info:".$page."&num=100&filter=0";
			echo "Taking $url\n";
			$r = $curl->raw_request($url, $timeout);
  			$pos = strpos($r, "Rank_");
  			if ($pos !== false) {
				$pagerank = substr($r, $pos + 9);
				$result = (int)$pagerank;
				break;
			} 			
  		}
  		return $result;  	
  }

	private function _check_proxy($page, $proxies, $tries = 5, $timeout = 30) {
		$cnt = 0; 
		$result = -1;
  		while ($cnt < $tries) {
  			$cnt++;
  			shuffle($proxies);
			$_tmp = explode('::', $proxies[0]); 
			$cproxy = explode(':', $_tmp[0]);
  			$socks = new socks5($cproxy[0], $cproxy[1], $timeout);
			
			$r = '';
			if ($socks->connect('toolbarqueries.google.com', 80)) {
				$out = "GET /tbr?client=navclient-auto&ch=".$this->checkHash($this->createHash($page)).
				"&features=Rank&q=info:".$page."&num=100&filter=0 HTTP/1.1\r\n";
				$out .= "Host: toolbarqueries.google.com\r\n";
				$out .= "User-Agent: Mozilla/4.0 (compatible; GoogleToolbar 2.0.114-big; Windows XP 5.1)\r\n";
				$out .= "Connection: Close\r\n\r\n";
				$r = $socks->send($out);
			}

  			$pos = strpos($r, "Rank_");
  			if ($pos !== false) {
				$pagerank = substr($r, $pos + 9);
				$result = (int)$pagerank;
				break;
			} 			
  		}
  		return $result;  	
  }
  
   private function fsockopen_ip ($host, $port, $tmp, $error, $connect_timeout, $bind_ip) {
		if (function_exists('stream_context_create') && function_exists('stream_socket_client')) {
			$socket_options = array('socket' => array('bindto' => $bind_ip.':0'));
			$socket_context = stream_context_create($socket_options);
			$socket = stream_socket_client($host.":".$port, $tmp, $error, $connect_timeout, STREAM_CLIENT_CONNECT, $socket_context);
		} else {
			$socket = @fsockopen($host, $port, $tmp, $error, $connect_timeout);
		}
		return $socket;
	}
}

?>
