<?php
$this->breadcrumbs=array(
	'Пользователи'=>array('index'),
	$model->login=>array('view','id'=>$model->id),
	'Редактировать',
);

$this->menu=array(
	array('label'=>'Список', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Просмотр', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление', 'url'=>array('admin')),
);
?>

<h2>Редактировать пользователя '<?php echo $model->login; ?>'</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>