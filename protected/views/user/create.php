<?php
$this->breadcrumbs=array(
	'Пользователи'=>array('index'),
	'Добавить',
);

$this->menu=array(
	array('label'=>'Список', 'url'=>array('index')),
	array('label'=>'Управление', 'url'=>array('admin')),
);
?>

<h2>Добавить пользователя</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>