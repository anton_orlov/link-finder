<?php
$this->breadcrumbs=array(
	'Пользователи',
);

$this->menu=array(
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Управление', 'url'=>array('admin')),
);
?>

<h2>Пользователи</h2>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'emptyText' => 'Ничего не найдено',
	'summaryText'=>Yii::t('labels', 'Результаты {start}-{end} из {count}'),
	'pager' => array(
		'class'=>'CLinkPager',
		'header' => 'Страницы:',
		'prevPageLabel' => 'Назад',
		'nextPageLabel' => 'Вперед',
	),	
)); ?>
