<?php
$this->breadcrumbs = array(
	'Пользователи' => array('index'),
	'Управление',
);

$this->menu=array(
	array('label'=>'Список', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

?>

<h2>Управление пользователями</h2>

<p>Вы можете дополнительно использовать операторы сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> or <b>=</b>) в начале каждого поля поиска.</p>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'	=>	$model->search(),
	'emptyText'		=> 'Ничего не найдено',
	'summaryText'	=> Yii::t('labels', 'Результаты {start}-{end} из {count}'),
	//'template' => '{summary}, {items} and {pager}.',
	'pager' => array(
		'class'				=>'CLinkPager',
		'header'				=> 'Страницы:',
		'prevPageLabel'	=> 'Назад',
		'nextPageLabel'	=> 'Вперед',
	),		
	'filter'=>$model,
	'columns'=>array(
		'id',
		'login',
		'fullname',
		'role',
		array(
			'class'=>'CButtonColumn',
			'buttons' => array (
				'update' => array(
					'label' => 'Редактировать',
				),
				'view' => array(
					'label' => 'Просмотр',
				),
				'delete' => array(
					'label' => 'Удалить',
				),
			),
		),
	),
)); ?>
