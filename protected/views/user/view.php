<?php
$this->breadcrumbs=array(
	'Пользователи' => array('index'),
	$model->login,
);

$this->menu=array(
	array('label'=>'Список', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Редактировать', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы уверены, что хотите удалить эту позицию?')),
	array('label'=>'Управление', 'url'=>array('admin')),
);
?>

<h2>Пользователь '<?php echo $model->login; ?>'</h2>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'login',
		'fullname',
		'role',
	),
)); ?>
