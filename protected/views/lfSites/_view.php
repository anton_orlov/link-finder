<div class="view">
	<?php echo CHtml::link(CHtml::encode($data->url), array('view', 'id'=>$data->id)); ?>
	<span style="float: right;"><?php echo CHtml::encode(Yii::app()->params['langs'][$data->lang]); ?></span>
</div>
