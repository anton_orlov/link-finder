<?php
$this->breadcrumbs=array(
	'Сайты'	=>	array('index'),
	'Добавить',
);

$this->menu=array(
	array('label' => 'Список', 'url'=>array('index')),
	array('label' => 'Управление', 'url'=>array('admin')),
);
?>

<h3>Добавить сайты</h3>

<?php echo $this->renderPartial('_form_create', array('model'=>$model)); ?>
