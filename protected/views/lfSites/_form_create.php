<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id' => 'lf-sites-form',
	'enableAjaxValidation' => false,
)); ?>

	<p class="note">Поля <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'Urls'); ?>
		<?php echo $form->textArea($model, 'urls', array('maxlength' => 300, 'rows' => 10, 'cols' => 60)); ?>
		<?php echo $form->error($model, 'urls'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lang'); ?>
		<?php //echo $form->textField($model,'lang',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->dropDownList($model, 'lang', Yii::app()->params['langs']); ?>
		<?php echo $form->error($model,'lang'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
