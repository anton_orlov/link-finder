<?php
$this->breadcrumbs=array(
	'Сайты' => array('index'),
	$model->url,
);

$this->menu=array(
	array('label'=>'Список', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Редактировать', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы уверены, что хотите удалить эту позицию?')),
	array('label'=>'Управление', 'url'=>array('admin')),
);
?>

<h2>Просмотр '<?php echo $model->url; ?>'</h2>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'url',
		array(
			'name' 	=> 'lang',
			'value'	=> Yii::app()->params['langs'][$model->lang],
		),
	),
)); ?>
