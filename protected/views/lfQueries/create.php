<?php
$this->breadcrumbs=array(
	'Запросы'	=>	array('index'),
	'Добавить',
);

$this->menu=array(
	array('label' => 'Список', 'url'=>array('index')),
	array('label' => 'Управление', 'url'=>array('admin')),
);
?>

<h2>Добавить запросы</h2>

<?php echo $this->renderPartial('_form_create', array('model'=>$model)); ?>
