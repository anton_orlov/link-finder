<?php
$this->breadcrumbs=array(
	'Запросы'=>array('index'),
	$model->query=>array('view','id'=>$model->id),
	'Редактировать',
);

$this->menu=array(
	array('label'	=>	'Список', 'url'=>array('index')),
	array('label'	=>	'Добавить', 'url'=>array('create')),
	array('label'	=>	'Просмотр', 'url'=>array('view', 'id'=>$model->id)),
	array('label'	=>	'Управление', 'url'=>array('admin')),
);
?>

<h2>Редактировать '<?php echo $model->query; ?>'</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
