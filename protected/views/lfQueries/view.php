<?php
$this->breadcrumbs=array(
	'Запросы' => array('index'),
	$model->query,
);

$this->menu=array(
	array('label'=>'Список', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Редактировать', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы уверены, что хотите удалить эту позицию?')),
	array('label'=>'Управление', 'url'=>array('admin')),
);
?>

<h2>Просмотр '<?php echo $model->query; ?>'</h2>

<?php
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'query',
		array(
			'name' => 'lang',
			'value' => Yii::app()->params['langs'][$model->lang],
		),
		array(
			'name'	=> "Всего / Готово / PR",
			'value'	=> $model->stat_total . " / " . $model->stat_done . " / " . $model->stat_haspr,	
		),
	),
)); ?>

<br /><br />
<h3>Страницы по запросу <i><?php echo $model->query; ?></i></h3>

<?php

$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'	=>	$pagesdataProvider,
	'columns'=>array(
		array(
			'name' => 'url',
         'value' => $data->url,
		),
		array(
			'name' => 'pr',
         'value' => $data->pr,
         'htmlOptions'=>array('width'=>'20px'),
		),	
	),
	//'itemView'		=>	'/lfPages/_view',
	'emptyText' => 'Ничего не найдено',
	'summaryText'=>Yii::t('labels', 'Результаты {start}-{end} из {count}'),
	'pager' => array(
		'class'=>'CLinkPager',
		'header' => 'Страницы:',
		'prevPageLabel' => 'Назад',
		'nextPageLabel' => 'Вперед',
	),

	));

?>
