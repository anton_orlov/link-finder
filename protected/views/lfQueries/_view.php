<div class="view">
	<?php echo CHtml::link(CHtml::encode($data->query), array('view', 'id'=>$data->id)); ?>
	<span style="float: right;"><?php echo CHtml::encode(Yii::app()->params['langs'][$data->lang]); ?> &nbsp; (<?php echo $data->stat_total; ?>/<?php echo $data->stat_done; ?>/<?php echo $data->stat_haspr; ?>)</span>
</div>
