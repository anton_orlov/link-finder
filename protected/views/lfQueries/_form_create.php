<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id' => 'lf-queries-form',
	'enableAjaxValidation' => false,
)); ?>

	<p class="note">Поля <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Запросы'); ?>
		<?php echo $form->textArea($model, 'queries', array('maxlength' => 300, 'rows' => 10, 'cols' => 60)); ?>
		<?php echo $form->error($model,'queries'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lang'); ?>
		<?php echo $form->dropDownList($model, 'lang', Yii::app()->params['langs']); ?>
		<?php echo $form->error($model,'lang'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
