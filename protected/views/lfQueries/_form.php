<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id' => 'lf-queries-form',
	'enableAjaxValidation' => false,
)); ?>

	<p class="note">Поля <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'query'); ?>
		<?php echo $form->textField($model,'query',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'query'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lang'); ?>
		<?php echo $form->dropDownList($model, 'lang', Yii::app()->params['langs']); ?>
		<?php echo $form->error($model,'lang'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
