<?php
$this->breadcrumbs=array(
	'Запросы',
);

$this->menu=array(
	array('label'	=> 'Добавить', 'url'=>array('create')),
	array('label'	=> 'Управление', 'url'=>array('admin')),
);

$this->lang_menu = array();
foreach (Yii::app()->params['langs'] as $lang_code => $lang_name) {
	$this->lang_menu[] = array(
		'label'	=> $lang_name, 'url'=>array('index&lang='.$lang_code)	
	);
}


?>

<h2>Запросы<?php if (!empty($lang)) { echo " по <i>".Yii::app()->params['langs'][$lang] . "</i>";   } ?></h2>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'emptyText' => 'Ничего не найдено',
	'summaryText'=>Yii::t('labels', 'Результаты {start}-{end} из {count}'),
	'pager' => array(
		'class'=>'CLinkPager',
		'header' => 'Страницы:',
		'prevPageLabel' => 'Назад',
		'nextPageLabel' => 'Вперед',
	),
)); ?>
