<?php

$this->pageTitle=Yii::app()->name . ' - Добавить ::NAME::';
$this->breadcrumbs=array(
	'::BNAME::'	=>	array('index'),
	'Добавить',
);

$this->menu=array(
	array('label' => 'Список ::NAME::', 'url'=>array('index')),
	array('label' => 'Управление ::NAME::', 'url'=>array('admin')),
);
?>

<h2>Добавить ::NAME::</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>