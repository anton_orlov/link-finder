<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id' => 'lf-pages-form',
	'enableAjaxValidation' => false,
)); ?>

	<p class="note">Поля <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'query_id'); ?>
		<?php echo $form->textField($model,'query_id'); ?>
		<?php echo $form->error($model,'query_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'site_id'); ?>
		<?php echo $form->textField($model,'site_id'); ?>
		<?php echo $form->error($model,'site_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'serp_position'); ?>
		<?php echo $form->textField($model,'serp_position'); ?>
		<?php echo $form->error($model,'serp_position'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pr'); ?>
		<?php echo $form->textField($model,'pr'); ?>
		<?php echo $form->error($model,'pr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pr_recheck'); ?>
		<?php echo $form->textField($model,'pr_recheck'); ?>
		<?php echo $form->error($model,'pr_recheck'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->