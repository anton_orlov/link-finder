<?php

$this->pageTitle=Yii::app()->name . ' - Редактировать ::NAME:: '.::BFIELD::;
$this->breadcrumbs=array(
	'::BNAME::'=>array('index'),
	::BFIELD::=>array('view','id'=>$model->id),
	'Редактировать',
);

$this->menu=array(
	array('label'	=>	'Список ::NAME::', 'url'=>array('index')),
	array('label'	=>	'Добавить ::NAME::', 'url'=>array('create')),
	array('label'	=>	'Просмотр ::NAME::', 'url'=>array('view', 'id'=>$model->id)),
	array('label'	=>	'Управление ::NAME::', 'url'=>array('admin')),
);
?>

<h2>Редактировать '::FIELD::'</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>