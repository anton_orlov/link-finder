<div class="wide form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'url'); ?>
		<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'query_id'); ?>
		<?php echo $form->textField($model,'query_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'site_id'); ?>
		<?php echo $form->textField($model,'site_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'serp_position'); ?>
		<?php echo $form->textField($model,'serp_position'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pr'); ?>
		<?php echo $form->textField($model,'pr'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pr_recheck'); ?>
		<?php echo $form->textField($model,'pr_recheck'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Найти'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->