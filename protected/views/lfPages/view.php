<?php

$this->pageTitle=Yii::app()->name . ' - Просмотр ::NAME:: '.::BFIELD::;
$this->breadcrumbs=array(
	'::BNAME::' => array('index'),
	::BFIELD::,
);

$this->menu=array(
	array('label'=>'Список ::NAME::', 'url'=>array('index')),
	array('label'=>'Добавить ::NAME::', 'url'=>array('create')),
	array('label'=>'Редактировать ::NAME::', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить ::NAME::', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы уверены, что хотите удалить данный элемент?')),
	array('label'=>'Управление ::NAME::', 'url'=>array('admin')),
);
?>

<h2>Просмотр '::FIELD::'</h2>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'url',
		'query_id',
		'site_id',
		'serp_position',
		'pr',
		'pr_recheck',
	),
)); ?>
