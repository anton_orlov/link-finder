<?php

$this->pageTitle=Yii::app()->name . ' - Список ::NAME::';
$this->breadcrumbs=array(
	'::BNAME::',
);

$this->menu=array(
	array('label'	=> 'Добавить ::NAME::', 'url'=>array('create')),
	array('label'	=> 'Управление ::NAME::', 'url'=>array('admin')),
);
?>

<h2>::BNAME::</h2>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'emptyText' => 'Ничего не найдено',
	'summaryText'=>Yii::t('labels', 'Результаты {start}-{end} из {count}'),
	'pager' => array(
		'class'=>'CLinkPager',
		'header' => 'Страницы:',
		'prevPageLabel' => 'Назад',
		'nextPageLabel' => 'Вперед',
	),
)); ?>
