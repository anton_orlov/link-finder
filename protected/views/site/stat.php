<?php
$this->pageTitle=Yii::app()->name . ' - Статистика';
$this->breadcrumbs=array(
	'Статистика',
);
?>

<h2>Статистика</h2>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$data,
	'attributes'=>array(
		array(
			'name' 	=> 'Запросы',
			'value'	=> $data['queries']['total'].' / '.$data['queries']['done'],
		),
		array(
			'name' 	=> 'Проверка PR',
			'value'	=> $data['pr']['total'].' / '.$data['pr']['done'],
		),
	),
)); ?>
