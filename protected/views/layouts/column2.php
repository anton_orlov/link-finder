<?php $this->beginContent('//layouts/main'); ?>
<div class="span-19">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="span-5 last">
	<div id="sidebar">
	<?php
	
	if (count($this->lang_menu) > 0) {
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Языки',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->lang_menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
	}
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Действия',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();

	
	
?>
	
	</div><!-- sidebar -->
</div>
<?php $this->endContent(); ?>
