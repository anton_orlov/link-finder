<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->
<?php $isGuest = Yii::app()->user->isGuest ?>
	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Войти', 'url'=>array('/login/index'), 'visible'=> $isGuest),
				array('label'=>'Главная',	'url' => array('/site/index'), 'visible'=> !$isGuest),
				array('label'=>'Запросы', 'url'=>array('/lfQueries/index'), 'visible'=> !$isGuest),
				array('label'=>'Сайты',	'url' => array('/lfSites/index'), 'visible'=> !$isGuest),
				array('label'=>'Пользователи', 'url'=>array('/user/index'), 'visible'=> !$isGuest),
				array('label'=>'Статистика', 'url'=>array('/site/stat'), 'visible'=> !$isGuest),
				/*array('label'=>'О нас',	'url' => array('/site/page', 'view'=>'about')),
				array('label'=>'Контакты',	'url' => array('/site/contact'), 'visible'=> !Yii::app()->user->isGuest),*/			
				array('label'=>'Выйти ('.Yii::app()->user->fullname.')', 'url'=>array('/login/logout'), 'visible'=> !$isGuest),
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by Anton Orlov<br/>
		<?php //echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
