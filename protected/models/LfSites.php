<?php

/**
 * This is the model class for table "lf_sites".
 *
 * The followings are the available columns in table 'lf_sites':
 * @property integer $id
 * @property string $url
 * @property string $lang
 */
class LfSites extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LfSites the static model class
	 */
	 
	public $urls = '';
	 
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lf_sites';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('url, lang', 'required'),
			array('done', 'numerical', 'integerOnly'=>true),
			array('url', 'length', 'max'=>255),
			array('lang', 'in', 'range' => array_keys(Yii::app()->params['langs'])),
			array('url', 'unique'),			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, url, lang, done', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'url' => 'Url',
			'lang' => 'Язык',
			'done' => 'Готов',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('lang',$this->lang,true);
		$criteria->compare('done',$this->done);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize'=> 30,
			)
		));
	}
}
