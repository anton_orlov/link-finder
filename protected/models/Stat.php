<?php

/**
*
*
*
**/
class Stat extends CComponent
{
	public function getQueriesStat ()
	{
		$db = Yii::app()->db;
		$total = $db->createCommand("SELECT COUNT(*) FROM {{tasks}}")
					->queryScalar();
		$done = $db->createCommand("SELECT COUNT(*) FROM {{tasks}} WHERE done = 1")
					->queryScalar();
		return array(
			'total' => $total,
			'done' => $done,
		);
	}

	public function getPrStat ()
	{
		$db = Yii::app()->db;
		$total = $db->createCommand("SELECT COUNT(*) FROM {{pages}}")
					->queryScalar();
		$done = $db->createCommand("SELECT COUNT(*) FROM {{pages}} WHERE pr != -1")
					->queryScalar();
		return array(
			'total' => $total,
			'done' => $done,
		);
	}
}
