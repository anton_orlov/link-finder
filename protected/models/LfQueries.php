<?php

/**
 * This is the model class for table "lf_queries".
 *
 * The followings are the available columns in table 'lf_queries':
 * @property integer $id
 * @property string $query
 * @property string $lang
 * @property integer $done
 */
class LfQueries extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LfQueries the static model class
	 */

	public $queries = '';
	public $stat_total;
	public $stat_done;
	public $stat_haspr;
	public $isNewRec = 0;
	

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lf_queries';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('query, lang', 'required'),
			array('query', 'length', 'max'=>255),
			array('lang', 'in', 'range' => array_keys(Yii::app()->params['langs'])),
			//array('query', 'unique'),	
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, query, lang', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user_ids' => array(self::HAS_MANY, 'UserQueries', 'query_id')
    	);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'query' => 'Запрос',
			'lang' => 'Язык',
		);
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
		   {
		   	$lang = $this->lang;
		   	$db = Yii::app()->db;
		   	$db->createCommand()
		   		->update('{{sites}}', array(
		   			'done' => 0
		   		),
						'lang="'.$lang.'"'
					);
				
        } else {
        			$this->checkUserRights();   	
        }
        return true;
    }
    else
        return false;
	}
	
	protected function afterSave() {
		if($this->isNewRec)
		{
			$this->addUserId();
		}
      return true;
	}
	
	public function addUserId($query_id = 0) {
		if (!$query_id)
			$query_id = $this->primaryKey;
		$rel = new UserQueries;
		$user_id = Yii::app()->user->id;
		if (!$rel->exists("query_id=:query_id AND user_id=:user_id", array(':query_id' => $query_id, ':user_id' => $user_id))) {
			$rel->user_id = $user_id;
			$rel->query_id = $query_id;
			$rel->save();			
		}
	}
	
	protected function afterFind() {
		$this->checkUserRights();
		$db = Yii::app()->db;
			
		$this->stat_total = $db->createCommand("SELECT COUNT(*) FROM {{pages}} WHERE query_id = {$this->id}")
					->queryScalar();
		$this->stat_done = $db->createCommand("SELECT COUNT(*) FROM {{pages}} WHERE pr != -1 AND query_id = {$this->id}")
					->queryScalar();
		$this->stat_haspr = $db->createCommand("SELECT COUNT(*) FROM {{pages}} WHERE pr > 0 AND query_id = {$this->id}")
					->queryScalar();

		return true;
	}
	
	public function findQueryId ($query = '', $lang = '') {
		$db = Yii::app()->db;
		return $db->createCommand()
			->select('id')
			->from("{{queries}}")
			->where(array('AND', 'lang=:lang', 'query=:query'))
			->bindParam(":lang", $lang)
			->bindParam(":query", $query)
			->queryScalar();
	}
	
	protected function checkUserRights () {
		if (Yii::app()->user->getState('role') != "admin" && !in_array(Yii::app()->user->id, $this->getUserIds())) {
			throw new CHttpException(403, 'У вас недостаточно прав для выполнения указанного действия.');
		}	
	}
	
	protected function getUserIds () {
		$result = array();
		foreach ($this->user_ids as $ar) {
			$result[] = $ar->user_id;		
		}
		return $result;	
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('query',$this->query,true);
		$criteria->compare('lang',$this->lang,true);
		
		if (Yii::app()->user->getState('role') != "admin") {
			$criteria->mergeWith(array(
				'condition' => 'user_ids.user_id = ' . Yii::app()->user->id,
				'with' => array (
					'user_ids' => array(
						'alias'=>'user_ids',
						'together'=> 1,
					),
				),
			));
		}
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize'=> 30,
			)
		));
	}
}
