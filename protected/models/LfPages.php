<?php

/**
 * This is the model class for table "{{pages}}".
 *
 * The followings are the available columns in table '{{pages}}':
 * @property integer $id
 * @property string $url
 * @property integer $query_id
 * @property integer $site_id
 * @property integer $serp_position
 * @property integer $pr
 * @property integer $pr_recheck
 */
class LfPages extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LfPages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pages}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('url, query_id, site_id, serp_position', 'required'),
			array('query_id, site_id, serp_position, pr, pr_recheck', 'numerical', 'integerOnly'=>true),
			array('url', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, url, query_id, site_id, serp_position, pr, pr_recheck', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'url' => 'Url',
			'query_id' => 'Query',
			'site_id' => 'Site',
			'serp_position' => 'Serp Position',
			'pr' => 'Pr',
			'pr_recheck' => 'Pr Recheck',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('query_id',$this->query_id);
		$criteria->compare('site_id',$this->site_id);
		$criteria->compare('serp_position',$this->serp_position);
		$criteria->compare('pr',$this->pr);
		$criteria->compare('pr_recheck',$this->pr_recheck);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}